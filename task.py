import json
from datetime import datetime


def task1():
    # opening json file for reading
    with open("employee.json") as e:
        data = json.load(e)
    # doing thing based on user input
    value = input("enter employee name")
    policy = input("Enter employee policy name")
    fromdate = input("enter from date")
    indate = input("enter in date")
    # appending new values
    data[value].append({"from": fromdate,
                        "to": indate,
                        "profile": policy})
    with open("employeetemp.json", "w") as w:
        json.dump(data, w, indent=4),


def task2():
    Emp = input("Enter employee  name")
    input_date = input("Enter date to know applied policy")
    policy2 = None

    with open("employeetemp.json") as e2:
        data2 = json.load(e2)

    input_date = datetime.strptime(input_date, "%d-%m-%Y")
    length = len(data2[Emp])
    for ia in range(0, length):
        date_time_from_obj = datetime.strptime(data2[Emp][ia]['from'], "%d-%m-%Y")
        date_time_to_obj = datetime.strptime(data2[Emp][ia]['to'], "%d-%m-%Y")

        if input_date >= date_time_from_obj and input_date <= date_time_to_obj:
            policy2 = data2[Emp][ia]['profile']

    print(policy2)

if __name__=='__main__':
    task1()
    task2()
